/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;

import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.EOFException;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InvalidObjectException;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author ERICK-PC
 */
public class Grafico {

    public DefaultCategoryDataset configuracaoDataSerGrafico(DefaultCategoryDataset ds, double valorMedia,String caso, int contadorExecucao) throws InvalidObjectException, FileNotFoundException, IOException {
        // cria o conjunto de dados
        ds.addValue(valorMedia, caso, String.valueOf(contadorExecucao));
       return ds ;
    }
// cria o gráfico
    public void mostraGrafico(DefaultCategoryDataset ds) throws IOException{
        JFreeChart grafico = ChartFactory.createBarChart3D("Resultados das 5000 Iterações", "Casos", "Médias das diferenças com relação a palavra alvo", ds, PlotOrientation.VERTICAL, true, true, false);
        OutputStream arquivo = new FileOutputStream("grafico.png");
        ChartUtilities.writeChartAsPNG(arquivo, grafico, 1024, 768);
        arquivo.close();
    }
}
