import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class PalavraAlvo implements Palavra {
	private List<Double> letrasNumericas = new ArrayList<>();
	private List<Double> toleranciaParaMenos = new ArrayList<>();
	private List<Double> toleranciaParaMais = new ArrayList<>();


	public void adicionaValores(double valor) {
		// TODO Auto-generated method stub
		this.letrasNumericas.add(valor) ;
	}

	public int tamanho() {
		// TODO Auto-generated method stub
		return this.letrasNumericas.size();
	}
	
	public double getLetra(int indice){
		return this.letrasNumericas.get(indice);
	}

	public void gerarToleranciaPalavraAlvo() {
		// TODO Auto-generated method stub
		double valorMinimo = 0.00 ;
		double valorMaximo = 0.00 ;
		double valorMinimoFormatado = 0.00 ;
		double valorMaximoFormatado = 0.00 ;
		for(int i = 0 ; i < this.tamanho() ; i ++){
			valorMinimo = this.getLetra(i) - (this.getLetra(i) * 0.05) ;
			valorMinimoFormatado = this.formataCasasDecimais(valorMinimo);
			this.toleranciaParaMenos.add(valorMinimoFormatado);
		}
		for(int i = 0 ; i < this.tamanho() ; i ++){
			valorMaximo = this.getLetra(i) + (this.getLetra(i) * 0.05) ;
			valorMaximoFormatado = this.formataCasasDecimais(valorMaximo);
			this.toleranciaParaMais.add(valorMaximoFormatado);
		}
	}

	public double getToleranciaParaMenos(int indice) {
		// TODO Auto-generated method stub
		double valor = this.toleranciaParaMenos.get(indice);
		return valor;
	}
	
	public double formataCasasDecimais(double valor) {
		// TODO Auto-generated method stub
		DecimalFormat df = new DecimalFormat("#.##"); 
		String numeroString = df.format(valor);
		double numeroDouble = Double.parseDouble(numeroString.replace(",","."));
		return numeroDouble;
	}
	
	public double getToleranciaParaMais(int indice) {
		// TODO Auto-generated method stub
		double valor = this.toleranciaParaMais.get(indice);
		return valor;
	}
	
	public double getSoma() {
		// TODO Auto-generated method stub
		double soma = 0.00 ;
		for(int i = 0 ; i < this.tamanho() ; i++){
			soma += this.getLetra(i);
		}
		return soma ;
	}

	public double getMaiorValorToleranciaParaMais() {
		// TODO Auto-generated method stub
		double maior = 0.00,temp = 0.00 ;
		for(int i = 0 ; i < this.toleranciaParaMais.size(); i++){
			temp = this.getToleranciaParaMais(i);
			if(temp > maior){
				maior = temp ;
			}
		}
		return maior ;
	}

	public double getMenorValorToleranciaParMenos() {
		// TODO Auto-generated method stub
		double menor = 0.00,temp = 0.00 ;
		for(int i = 0 ; i < this.toleranciaParaMenos.size(); i++){
			temp = this.getToleranciaParaMenos(i);
			if(temp < menor){
				menor = temp ;
			}
		}
		return menor ;
	}
}
