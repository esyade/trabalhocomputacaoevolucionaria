
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.jfree.data.category.DefaultCategoryDataset;

public class Principal {

    static Random random = new Random();

    public static void main(String[] args) throws InvalidObjectException, IOException {
        // TODO Auto-generated method stub
        double variancia = 0.22;
        Grafico grafico = new Grafico();
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
        ControleDeQualidadeDePalavrasDeTeste controladorDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
        PalavraAlvo palavraAlvoDaVez = new PalavraAlvo();
        PalavraDeTeste melhorSolucaoPorIteracao  = new PalavraDeTeste();
        Gerador gerador = new Gerador();
        int tamanhoDaPalavra = 0;
        int quantidadeDePalavrasDeTeste = 0;
        for (int i = 1; i <= 4; i++) {
            if (i == 1) {
                PalavraAlvo palavraAlvo = new PalavraAlvo();
                palavraAlvo.adicionaValores(0.77);
                palavraAlvo.adicionaValores(0.25);
                palavraAlvo.adicionaValores(0.43);
                palavraAlvo.adicionaValores(0.97);
                palavraAlvo.adicionaValores(0.32);
                tamanhoDaPalavra = palavraAlvo.tamanho();
                palavraAlvoDaVez = palavraAlvo;
                quantidadeDePalavrasDeTeste = 50;
            } else if (i == 2) {
                PalavraAlvo palavraAlvo = new PalavraAlvo();
                palavraAlvo.adicionaValores(0.12);
                palavraAlvo.adicionaValores(0.56);
                palavraAlvo.adicionaValores(0.28);
                palavraAlvo.adicionaValores(0.73);
                palavraAlvo.adicionaValores(0.30);
                tamanhoDaPalavra = palavraAlvo.tamanho();
                palavraAlvoDaVez = palavraAlvo;
                quantidadeDePalavrasDeTeste = 100;
            } else if (i == 3) {
                PalavraAlvo palavraAlvo = new PalavraAlvo();
                palavraAlvo.adicionaValores(0.3);
                palavraAlvo.adicionaValores(0.2);
                palavraAlvo.adicionaValores(0.4);
                palavraAlvo.adicionaValores(0.9);
                palavraAlvo.adicionaValores(0.3);
                palavraAlvo.adicionaValores(0.2);
                palavraAlvo.adicionaValores(0.6);
                palavraAlvo.adicionaValores(0.8);
                palavraAlvo.adicionaValores(0.3);
                palavraAlvo.adicionaValores(0.11);
                tamanhoDaPalavra = palavraAlvo.tamanho();
                palavraAlvoDaVez = palavraAlvo;
                quantidadeDePalavrasDeTeste = 50;
            } else if (i == 4) {
                PalavraAlvo palavraAlvo = new PalavraAlvo();
                palavraAlvo.adicionaValores(0.7);
                palavraAlvo.adicionaValores(0.52);
                palavraAlvo.adicionaValores(0.48);
                palavraAlvo.adicionaValores(0.94);
                palavraAlvo.adicionaValores(0.34);
                palavraAlvo.adicionaValores(0.72);
                palavraAlvo.adicionaValores(0.23);
                palavraAlvo.adicionaValores(0.45);
                palavraAlvo.adicionaValores(0.17);
                palavraAlvo.adicionaValores(0.13);
                tamanhoDaPalavra = palavraAlvo.tamanho();
                palavraAlvoDaVez = palavraAlvo;
                quantidadeDePalavrasDeTeste = 100;
            }
            System.out.println("n\n============================================================================ ");
            System.out.println("E X E M P L O    C A S O    T = " + palavraAlvoDaVez.tamanho() + "   N = " + quantidadeDePalavrasDeTeste + " //////////////////");
            System.out.println("============================================================================== ");
            gerador.addPalavraAlvo(palavraAlvoDaVez);
            for (int contadorExecucao = 1; contadorExecucao <= 5; contadorExecucao++) {
                System.out.println("\n\n---execucao----" + contadorExecucao + "-------------------/////////////////////\n\n");
                bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidadeDePalavrasDeTeste));
                List<PalavraDeTeste> melhoresSolucoesPorIteracao  = new ArrayList<>();
                ///////////////////////////////////////////////////////////
                for (int iteracao = 0; iteracao < 5000; iteracao++) {
                    System.out.println("\n\n---iteracao----" + iteracao + "------- Execucao -----"+contadorExecucao);
                    controladorDeQualidade.ordenaPalavrasDeTeste(bancoDePalavrasDeTeste);
                    bancoDePalavrasDeTeste = controladorDeQualidade.getBancoOrdenado();
                    bancoDePalavrasDeTeste.addListaMetadeMelhorDePalavrasDeTeste(controladorDeQualidade.extraiMetadeMelhorDasPalavras(bancoDePalavrasDeTeste));
                    bancoDePalavrasDeTeste.addListaMetadePiorDePalavrasDeTeste(controladorDeQualidade.extraiMetadePiorDasPalavras(bancoDePalavrasDeTeste));
                    bancoDePalavrasDeTeste.addListaMetadePiorDePalavrasDeTeste(controladorDeQualidade.substituiMetadePiorDePalavrasDeTeste(bancoDePalavrasDeTeste.getListaMetadePiorDePalavrasDeTeste(), bancoDePalavrasDeTeste.getListaMetadeMelhorDePalavrasDeTeste(), gerador, variancia, random));
                    melhorSolucaoPorIteracao = controladorDeQualidade.getMelhorSolucaoPorIteracao(bancoDePalavrasDeTeste.getListaMetadePiorDePalavrasDeTeste(), palavraAlvoDaVez);
                    melhoresSolucoesPorIteracao.add(melhorSolucaoPorIteracao);
                    bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(melhoresSolucoesPorIteracao);
                    System.out.println("Melhor palavra da iteração \n"+gerador.exibePalavraDeTeste(melhorSolucaoPorIteracao));
                    double soma = controladorDeQualidade.getSoma(bancoDePalavrasDeTeste.getListaDePalavrasDeTeste(),palavraAlvoDaVez);
                    double media = controladorDeQualidade.getMedia(soma,bancoDePalavrasDeTeste.getListaDePalavrasDeTeste()) ;
                    
                    ds = grafico.configuracaoDataSerGrafico(ds, media, "T" + palavraAlvoDaVez.tamanho() + "N" + quantidadeDePalavrasDeTeste, iteracao);
                    System.out.println("Palavra Alvo"+gerador.exibePalavraDeTeste(palavraAlvoDaVez));
                    System.out.println("Historico da melhor solução \n"+gerador.exibeLista(bancoDePalavrasDeTeste.getListaDePalavrasDeTeste()));
                    System.out.println("Soma ="+soma);
                    System.out.println("Média ="+media);
                    System.out.println("Soma  da Palavra Alvo"+palavraAlvoDaVez.getSoma());
                 
                    System.out.println("---------------------------------------------\n\n");
                    System.out.print("\n\n");
                  
                }
              
                //melhoresSolucoesPorExecucao.add(controladorDeQualidade.getMelhorSolucaoPorExecucao(melhoresSolucoesPorIteracao, palavraAlvoDaVez));
               // System.out.print(gerador.exibeLista(melhoresSolucoesPorExecucao));
              //   melhoresSolucoesPorExecucao = controladorDeQualidade.getMelhorSolucaoPorExecucao(palavrasDentroDoIntervalo, palavraAlvoDaVez);
               //     double media = controladorDeQualidade.getMedia(melhoresSolucoesPorExecucao, palavraAlvoDaVez);
               //     ds = grafico.configuracaoDataSerGrafico(ds, media, "T" + palavraAlvoDaVez.tamanho() + "N" + quantidadeDePalavrasDeTeste, iteracao);
               
            }
            ////analise de resultados
            
        }
         grafico.mostraGrafico(ds);


    }

}
