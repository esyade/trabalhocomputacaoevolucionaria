
import java.util.ArrayList;
import java.util.List;

public class PalavraDeTeste implements Palavra {

    private List<Double> letrasNumericas = new ArrayList<>();
    private double diferencaReferentePalavraAlvo;
    private int quantidadeDeValoresDentroDaToleranciaDaPalavraAlvo;

    @Override
    public void adicionaValores(double valor) {
        // TODO Auto-generated method stub
        this.letrasNumericas.add(valor);
    }

    @Override
    public int tamanho() {
        // TODO Auto-generated method stub
        return this.letrasNumericas.size();
    }

    @Override
    public double getLetra(int indice) {
        // TODO Auto-generated method stub
        return this.letrasNumericas.get(indice);
    }

    @Override
    public void gerarToleranciaPalavraAlvo() {
        // TODO Auto-generated method stub

    }

    @Override
    public double getToleranciaParaMenos(int indice) {
        // TODO Auto-generated method stub
        return 0.0;
    }

    @Override
    public double formataCasasDecimais(double valor) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public double getToleranciaParaMais(int indice) {
        // TODO Auto-generated method stub
        return 0.0;
    }

    public double getSoma() {
        // TODO Auto-generated method stub
        double soma = 0.00;
        for (int i = 0; i < this.tamanho(); i++) {
            soma += this.getLetra(i);
        }
        return soma;
    }

    public double getDiferencaReferentePalavraAlvo() {
        return diferencaReferentePalavraAlvo;
    }

    public void setDiferencaReferentePalavraAlvo(double qualidadeReferentePalavraAlvo) {
        this.diferencaReferentePalavraAlvo = qualidadeReferentePalavraAlvo;
    }

    public int getQuantidadeDeValoresDentroDaToleranciaPalavraAlvo() {
        return this.quantidadeDeValoresDentroDaToleranciaDaPalavraAlvo;
    }

    public void setQuantidadeDeValoresDentroDaTolerancialavraAlvo(int qualidadeReferenteToleranciaPalavraAlvo) {
        this.quantidadeDeValoresDentroDaToleranciaDaPalavraAlvo = qualidadeReferenteToleranciaPalavraAlvo;
    }

    public void calculaDiferencaReferentePalavraAlvo(PalavraAlvo palavraAlvo) {
        double diferenca = 0.0 ;
        if(palavraAlvo.getSoma() > this.getSoma()){
            diferenca = palavraAlvo.getSoma() - this.getSoma();
        }else{
            diferenca = this.getSoma() - palavraAlvo.getSoma();
        }
        this.diferencaReferentePalavraAlvo = diferenca;
    }

    public void calculaQuantidadeDeValoresDentroDaToleranciaDaPalavraAlvo(PalavraAlvo palavraAlvo) {
        int quantidade = 0;
        palavraAlvo.gerarToleranciaPalavraAlvo();
        for (int m = 0; m < this.tamanho(); m++) {
            if (palavraAlvo.getToleranciaParaMenos(m) <= this.getLetra(m) && palavraAlvo.getToleranciaParaMais(m) >= this.getLetra(m)) {
                quantidade++;
            }
        }

        this.quantidadeDeValoresDentroDaToleranciaDaPalavraAlvo = quantidade;
    }

}
