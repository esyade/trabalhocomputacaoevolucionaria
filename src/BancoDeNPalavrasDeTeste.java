import java.util.ArrayList;
import java.util.List;

public class BancoDeNPalavrasDeTeste {

	private List<PalavraDeTeste> listaPalavrasDeTeste ;
	private List<PalavraDeTeste> listaMelhoresPalavrasDeTeste ;
	private List<PalavraDeTeste> bdPioresPalavrasDeTeste ;

	public void adicionaPalavraTeste(PalavraDeTeste palavraDeTesteT5) {
		// TODO Auto-generated method stub
		this.listaPalavrasDeTeste = new ArrayList<>();
		this.listaPalavrasDeTeste.add(palavraDeTesteT5);
	}

	public int tamanhoBdPalavrasDeTeste() {
		// TODO Auto-generated method stub
		return listaPalavrasDeTeste.size();
	}

	public void addListaDePalavrasDeTeste(List<PalavraDeTeste> listaDePalavrasDeTeste) {
		// TODO Auto-generated method stub
		this.listaPalavrasDeTeste = listaDePalavrasDeTeste ;
	}
	
	public void addListaMetadePiorDePalavrasDeTeste(List<PalavraDeTeste> listaDePalavrasDeTeste) {
		// TODO Auto-generated method stub
		this.bdPioresPalavrasDeTeste = listaDePalavrasDeTeste ;
	}
	
	public void addListaMetadeMelhorDePalavrasDeTeste(List<PalavraDeTeste> listaDePalavrasDeTeste) {
		// TODO Auto-generated method stub
		this.listaMelhoresPalavrasDeTeste = listaDePalavrasDeTeste ;
	}
	
	public void addMelhorPalavraDeTeste(PalavraDeTeste melhorPalavraDeTeste) {
		// TODO Auto-generated method stub
		this.listaMelhoresPalavrasDeTeste.add(melhorPalavraDeTeste) ;
	}
	
	public void alteraPalavraDeTeste(int indice,PalavraDeTeste novaPalavraDeTeste) {
		// TODO Auto-generated method stub
		this.listaPalavrasDeTeste.set(indice,novaPalavraDeTeste) ;
	}
	
	public void addPiorPalavraDeTeste(PalavraDeTeste melhorPalavraDeTeste) {
		// TODO Auto-generated method stub
		this.listaMelhoresPalavrasDeTeste.add(melhorPalavraDeTeste) ;
	}

	public List<PalavraDeTeste> getListaDePalavrasDeTeste() {
		// TODO Auto-generated method stub
	return 	this.listaPalavrasDeTeste ;
	}
	
	public PalavraDeTeste getPalavraDeTeste(int indice) {
		// TODO Auto-generated method stub
		PalavraDeTeste palavraDeTeste = this.listaPalavrasDeTeste.get(indice);
		return palavraDeTeste ;
	}
	
	public List<PalavraDeTeste> getListaMetadePiorDePalavrasDeTeste() {
		// TODO Auto-generated method stub
		return 	this.bdPioresPalavrasDeTeste ;
	}
	
	public List<PalavraDeTeste> getListaMetadeMelhorDePalavrasDeTeste() {
		// TODO Auto-generated method stub
		return 	this.listaMelhoresPalavrasDeTeste ;
	}

	public String exibeLista() {
		// TODO Auto-generated method stub
		String resultado = "" ;
		double soma = 0.0 ;
		for(int i = 0 ; i < this.listaPalavrasDeTeste.size(); i ++){
			PalavraDeTeste palavra = this.listaPalavrasDeTeste.get(i) ;
			resultado += "A"+i+": ";
			for(int j = 0 ; j < palavra.tamanho() ; j++){
				resultado += "A"+i+"letra: "+palavra.getLetra(j)+"\n" ;
			}
		}
		return resultado ;
	}
	
	public String exibePiorLista() {
		// TODO Auto-generated method stub
		String resultado = "" ;
		double soma = 0.0 ;
		for(int i = 0 ; i < this.bdPioresPalavrasDeTeste.size(); i ++){
			PalavraDeTeste palavra = this.bdPioresPalavrasDeTeste.get(i) ;
				
			for(int j = 0 ; j < palavra.tamanho() ; j++){
				resultado += "A"+i+"letra-"+j+": "+palavra.getLetra(j)+"\n";
			}
		}
		return resultado ;
	}
	
	public String exibeMelhorLista() {
		// TODO Auto-generated method stub
		String resultado = "" ;
		double soma = 0.0 ;
		for(int i = 0 ; i < this.listaMelhoresPalavrasDeTeste.size(); i ++){
			PalavraDeTeste palavra = this.listaMelhoresPalavrasDeTeste.get(i) ;
			for(int j = 0 ; j < palavra.tamanho() ; j++){
				resultado += "A"+i+"letra-"+j+": "+palavra.getLetra(j)+"\n" ;
			}
		}
		return resultado ;
	}

	
	
	
}
