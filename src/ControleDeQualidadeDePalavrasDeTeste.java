
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ControleDeQualidadeDePalavrasDeTeste {

    private BancoDeNPalavrasDeTeste bdPalavrasDeTesteOrdenadas;

    public void ordenaPalavrasDeTeste(BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste) {
        // TODO Auto-generated method stub
        BancoDeNPalavrasDeTeste bdTemp = bancoDePalavrasDeTeste;
        Collections.sort(bdTemp.getListaDePalavrasDeTeste(), new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                PalavraDeTeste p1 = (PalavraDeTeste) o1;
                PalavraDeTeste p2 = (PalavraDeTeste) o2;
                return p1.getSoma() < p2.getSoma() ? -1 : (p1.getSoma() > p2.getSoma() ? +1 : 0);
            }
        });
        this.bdPalavrasDeTesteOrdenadas = bdTemp;
    }

    public BancoDeNPalavrasDeTeste getBancoOrdenado() {
        // TODO Auto-generated method stub
        return this.bdPalavrasDeTesteOrdenadas;
    }

    public String exibeBancoOrdenado() {
        // TODO Auto-generated method stub
        String resultado = "";
        double soma = 0.0;
        for (int i = 0; i < this.bdPalavrasDeTesteOrdenadas.tamanhoBdPalavrasDeTeste(); i++) {
            PalavraDeTeste palavra = this.bdPalavrasDeTesteOrdenadas.getPalavraDeTeste(i);
            soma = 0.00;
            for (int j = 0; j < palavra.tamanho(); j++) {
                soma += palavra.getLetra(j);
            }
            resultado += "A" + i + ": " + palavra.getLetra(0) + " | " + palavra.getLetra(1) + " | " + palavra.getLetra(2) + " | " + palavra.getLetra(3) + " | " + palavra.getLetra(4) + " | " + "SOMA = " + soma + "\n";
        }
        return resultado;
    }

    public List<PalavraDeTeste> extraiMetadePiorDasPalavras(BancoDeNPalavrasDeTeste bancoDePalavrasDeTesteOrdenadas) {
        // TODO Auto-generated method
        List<PalavraDeTeste> bdTemp = new ArrayList<>();
        for (int i = bancoDePalavrasDeTesteOrdenadas.tamanhoBdPalavrasDeTeste()-1; i != (bancoDePalavrasDeTesteOrdenadas.tamanhoBdPalavrasDeTeste()/2)-1; i--) {
            bdTemp.add(bancoDePalavrasDeTesteOrdenadas.getPalavraDeTeste(i));
        }
        return bdTemp;
    }

    public List<PalavraDeTeste> extraiMetadeMelhorDasPalavras(BancoDeNPalavrasDeTeste bancoDePalavrasDeTesteOrdenadas) {
        // TODO Auto-generated method
        List<PalavraDeTeste> listTemp = new ArrayList<>();
        for (int i = 0; i < (bancoDePalavrasDeTesteOrdenadas.tamanhoBdPalavrasDeTeste() / 2); i++) {
            listTemp.add(bancoDePalavrasDeTesteOrdenadas.getPalavraDeTeste(i));
        }
        return listTemp;
    }

    public List<PalavraDeTeste> substituiMetadePiorDePalavrasDeTeste(List<PalavraDeTeste> listaMetadePiorDePalavrasDeTeste, List<PalavraDeTeste> listaMetadeMelhorDePalavrasDeTeste, Gerador gerador, double variancia, Random random) {
        // TODO Auto-generated method stub
        List<PalavraDeTeste> listaDePalavrasGaussianas = new ArrayList<>();
        PalavraDeTeste palavraDaVez = new PalavraDeTeste();
        for (int i = 0; i < listaMetadePiorDePalavrasDeTeste.size(); i++) {
            palavraDaVez = listaMetadePiorDePalavrasDeTeste.get(i);
            PalavraDeTeste palavraGaussiana = new PalavraDeTeste();
            for (int j = 0; j < palavraDaVez.tamanho(); j++) {
                palavraGaussiana.adicionaValores(gerador.gerarValorPorGaussiana(palavraDaVez.getLetra(j), variancia, random));
            }
            listaMetadePiorDePalavrasDeTeste.set(i, palavraGaussiana);
        }
        listaDePalavrasGaussianas = listaMetadePiorDePalavrasDeTeste;
        return listaDePalavrasGaussianas;
    }

    private List<PalavraDeTeste> filtraPalavrasComValoresDentroDoIntervalo(PalavraAlvo palavraAlvo, List<PalavraDeTeste> listaMetadePiorDePalavrasDeTeste) {
        // TODO Auto-generated method stub
        palavraAlvo.gerarToleranciaPalavraAlvo();
        List<PalavraDeTeste> listaDasPalavrasDeTesteSolucoes = new ArrayList<>();
        int pontuacao = 0;
        PalavraDeTeste pMelhor = new PalavraDeTeste();
        for (PalavraDeTeste palavraDeTeste : listaMetadePiorDePalavrasDeTeste) {
            palavraDeTeste.calculaQuantidadeDeValoresDentroDaToleranciaDaPalavraAlvo(palavraAlvo);
            if (palavraDeTeste.getQuantidadeDeValoresDentroDaToleranciaPalavraAlvo() > 0) {
                listaDasPalavrasDeTesteSolucoes.add(palavraDeTeste);
                if(palavraDeTeste.getQuantidadeDeValoresDentroDaToleranciaPalavraAlvo() == palavraAlvo.tamanho()){
                    listaDasPalavrasDeTesteSolucoes = new ArrayList<>();
                    listaDasPalavrasDeTesteSolucoes.add(palavraDeTeste) ;
                    break ;
                }
            }
        }
        return listaDasPalavrasDeTesteSolucoes;
    }
    
    

    public PalavraDeTeste getMelhorSolucaoPorIteracao(List<PalavraDeTeste> listaMetadePiorDePalavrasDeTeste, PalavraAlvo palavraAlvo) {
        // TODO Auto-generated method stub
        double soma = 0.00, diferencaTemp = 0.0;
        PalavraDeTeste solucaoEscolhida = new PalavraDeTeste();
        solucaoEscolhida.setDiferencaReferentePalavraAlvo(10.0);
        List<PalavraDeTeste> listaMelhores = new ArrayList<>();
        listaMelhores = this.filtraPalavrasComValoresDentroDoIntervalo(palavraAlvo, listaMetadePiorDePalavrasDeTeste);
        for (PalavraDeTeste pTeste : listaMelhores) {
        	pTeste.calculaDiferencaReferentePalavraAlvo(palavraAlvo);
            if(pTeste.getDiferencaReferentePalavraAlvo() < solucaoEscolhida.getDiferencaReferentePalavraAlvo()){
                    solucaoEscolhida = pTeste ;
            }
        }
        return solucaoEscolhida;
    }

    public double getMedia(double soma,List<PalavraDeTeste> melhoresSolucoesPorIteracao) {
        // TODO Auto-generated method stub
        double media = 0.0 ;
        media = soma / melhoresSolucoesPorIteracao.size() ;
        return media;
    }

    public double getSoma(List<PalavraDeTeste> melhoresSolucoesPorIteracao, PalavraAlvo palavraAlvoDaVez) {
         double soma = 0.0;
        // for(int i = 0 ; i < palavraAlvoDaVez.tamanho() ; i++){
            for(PalavraDeTeste melhorPalavra : melhoresSolucoesPorIteracao){
                    melhorPalavra.calculaDiferencaReferentePalavraAlvo(palavraAlvoDaVez);
                    soma += melhorPalavra.getDiferencaReferentePalavraAlvo() ;
            }
        //  }
        return soma;
    }
    
//    public String getAcuracia(List<PalavraDeTeste> melhoresSolucoesPorIteracao, PalavraAlvo palavraAlvoDaVez) {
//         double soma = 0.0;
//         double maximo = 0.0 ;
//         double minimo = 100 ;
//         double valorTempMax = 0.0 ;
//         double valorTempMin = 0.0 ;
//         List<Double> listaDeDiferencas = new ArrayList<>();
//        // for(int i = 0 ; i < palavraAlvoDaVez.tamanho() ; i++){
//            for(PalavraDeTeste melhorPalavra : melhoresSolucoesPorIteracao){
//                    melhorPalavra.calculaDiferencaReferentePalavraAlvo(palavraAlvoDaVez);
//                    listaDeDiferencas .add(melhorPalavra.getDiferencaReferentePalavraAlvo()) ;
//            }
//            for(double valorDiferenca : listaDeDiferencas){
//                valorTempMax = valorDiferenca ;
//                if(valorTempMax > maximo){
//                    maximo = valorTempMax ;
//                }
//            }
//            for(double valorDiferenca2 : listaDeDiferencas){
//                if(valorTempMin < minimo){
//                    minimo = valorTempMin ;
//                }
//            }
//        //  }
//        return "acurácia média = "+ (minimo + maximo) / 2 ;
//    }
    
    

}
