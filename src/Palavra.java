
public interface Palavra {
	public void adicionaValores(double valor);
	public int tamanho();
	public double getLetra(int indice);
	public void gerarToleranciaPalavraAlvo();
	public double getToleranciaParaMenos(int indice);
	public double formataCasasDecimais(double valor);
	public double getToleranciaParaMais(int indice);
	public double getSoma() ;

}
