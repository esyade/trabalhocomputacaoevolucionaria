
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Gerador {

    private double valor;
    private PalavraAlvo palavraAlvo;
    static double nextNextGaussian = 0.0;
    static boolean haveNextNextGaussian = false;

    protected void setValor(double valor) {
        this.valor = valor;
    }

    public double gerarValor() {
        // TODO Auto-generated method stub
        double numero = (double) (Math.random());
        double numeroFormatado = this.formatDuasCadasDecimais(numero);
        return numeroFormatado;
    }

    public PalavraDeTeste gerarPalavraDeTeste() {
        // TODO Auto-generated method stub
        PalavraDeTeste listaDeValores = new PalavraDeTeste();
        for (int i = 0; i < this.palavraAlvo.tamanho(); i++) {
            double numeroFormatado = this.gerarValor();
            listaDeValores.adicionaValores(numeroFormatado);
        }
        return listaDeValores;
    }

    public double formatDuasCadasDecimais(double valor) {
        DecimalFormat df = new DecimalFormat("0.000");
        String numeroString = df.format(valor);
        double numeroDouble = Double.valueOf(numeroString.replace(",", "."));
        return numeroDouble;
    }

    public double gerarValorPorGaussiana(double media, double variancia, Random random) {
        // TODO Auto-generated method stub
        double numero = random.nextGaussian() * variancia + media;
        double numeroFormatado = this.formatDuasCadasDecimais(numero);
        return numeroFormatado;
    }

    public double getValor() {
        // TODO Auto-generated method stub
        return this.valor;
    }

    public List<PalavraDeTeste> gerarListaDePalavrasDeTeste(int quantidade) {
        // TODO Auto-generated method stub
        List<PalavraDeTeste> listTemp = new ArrayList<>();
        for (int i = 0; i < quantidade; i++) {
            PalavraDeTeste pTemp = new PalavraDeTeste();
            pTemp = gerarPalavraDeTeste();
            listTemp.add(pTemp);
        }
        return listTemp;
    }

    public String exibePalavraDeTeste(Palavra palavra) {
        // TODO Auto-generated method stub
        String resultado = "";
        double soma = 0.00;
        for (int i = 0; i < palavra.tamanho(); i++) {
            double letra = palavra.getLetra(i);
            resultado += letra + "|";
        }
        resultado += "\n";
        return resultado.replace(".", ",");
    }

    public String exibeLista(List<PalavraDeTeste> listaDePalavraDeTeste) {
        // TODO Auto-generated method stub
        String resultado = "";
        double soma = 0.0;
        for (int i = 0; i < listaDePalavraDeTeste.size(); i++) {
            PalavraDeTeste palavra = listaDePalavraDeTeste.get(i);
            for (int j = 0; j < palavra.tamanho(); j++) {
                resultado += palavra.getLetra(j) + "|";
            }
            resultado += "\n";
        }
        return resultado.replace(".", ",");
    }

    public void addPalavraAlvo(PalavraAlvo palavraAlvo2) {
        // TODO Auto-generated method stub
        this.palavraAlvo = palavraAlvo2;
    }

    public PalavraDeTeste gerarPalavraDeTesteGaussiana(double media, double variancia, Random random) {
        // TODO Auto-generated method stub
        PalavraDeTeste palavraDeTesteGaussiana = new PalavraDeTeste();
        for (int i = 0; i < this.palavraAlvo.tamanho(); i++) {
            double numeroFormatado = this.gerarValorPorGaussiana(media, variancia, random);
            palavraDeTesteGaussiana.adicionaValores(numeroFormatado);
        }
        return palavraDeTesteGaussiana;
    }

    public List<PalavraDeTeste> gerarListaDePalavrasDeTesteGaussianas(int quantidade, double media, double variancia, Random random) {
        // TODO Auto-generated method stub
        List<PalavraDeTeste> bdTemp = new ArrayList<>();
        for (int i = 0; i < quantidade; i++) {
            PalavraDeTeste pTemp = new PalavraDeTeste();
            pTemp = this.gerarPalavraDeTesteGaussiana(media, variancia, random);
            bdTemp.add(pTemp);
        }
        return bdTemp;
    }

}
