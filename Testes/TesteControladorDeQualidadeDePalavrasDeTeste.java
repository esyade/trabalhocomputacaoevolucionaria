
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;

public class TesteControladorDeQualidadeDePalavrasDeTeste {

    static Random random = new Random();

    @Test
    public void testeSomaDasPalavrasDeTeste() {
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
        Gerador gerador = new Gerador();
        PalavraAlvo palavraAlvo = new PalavraAlvo();
        palavraAlvo.adicionaValores(0.7);
        palavraAlvo.adicionaValores(0.2);
        palavraAlvo.adicionaValores(0.4);
        palavraAlvo.adicionaValores(0.9);
        palavraAlvo.adicionaValores(0.3);
        int quantidadeDePalavrasDeTeste = 100;
        gerador.addPalavraAlvo(palavraAlvo);
        bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidadeDePalavrasDeTeste));
        ControleDeQualidadeDePalavrasDeTeste controladorDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
        assertEquals(0, controladorDeQualidade.getSoma(bancoDePalavrasDeTeste.getListaDePalavrasDeTeste(), palavraAlvo),0);
    }
//	

    @Test
    public void testeOrdenaPalavrasDeTeste() {
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
        Gerador gerador = new Gerador();
        PalavraAlvo palavraAlvo = new PalavraAlvo();
        palavraAlvo.adicionaValores(0.7);
        palavraAlvo.adicionaValores(0.2);
        palavraAlvo.adicionaValores(0.4);
        palavraAlvo.adicionaValores(0.9);
        palavraAlvo.adicionaValores(0.3);
        int quantidadeDePalavrasDeTeste = 100;
        gerador.addPalavraAlvo(palavraAlvo);
        bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidadeDePalavrasDeTeste));
        BancoDeNPalavrasDeTeste banco = bancoDePalavrasDeTeste;
        ControleDeQualidadeDePalavrasDeTeste controladorDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
        controladorDeQualidade.ordenaPalavrasDeTeste(bancoDePalavrasDeTeste);
        assertEquals(0, controladorDeQualidade.exibeBancoOrdenado());
        //assertEquals(0,bancoDePalavrasDeTeste.exibeLista());

    }
//	

    @Test
    public void testeSeparaMetadePiorPalavras() {
    	List<PalavraDeTeste> listaMetadePiorPalavrasDeTeste = new ArrayList<>();
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
        Gerador gerador = new Gerador();
        PalavraAlvo palavraAlvo = new PalavraAlvo();
        palavraAlvo.adicionaValores(0.7);
        palavraAlvo.adicionaValores(0.2);
        palavraAlvo.adicionaValores(0.4);
        palavraAlvo.adicionaValores(0.9);
        palavraAlvo.adicionaValores(0.3);
        int quantidadeDePalavrasDeTeste = 100;
        gerador.addPalavraAlvo(palavraAlvo);
        bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidadeDePalavrasDeTeste));
        BancoDeNPalavrasDeTeste banco = bancoDePalavrasDeTeste;
        ControleDeQualidadeDePalavrasDeTeste controladorDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
        controladorDeQualidade.ordenaPalavrasDeTeste(bancoDePalavrasDeTeste);
        bancoDePalavrasDeTeste = controladorDeQualidade.getBancoOrdenado();
        listaMetadePiorPalavrasDeTeste = controladorDeQualidade.extraiMetadePiorDasPalavras(bancoDePalavrasDeTeste);
        bancoDePalavrasDeTeste.addListaMetadePiorDePalavrasDeTeste(listaMetadePiorPalavrasDeTeste);
        assertEquals(0,gerador.exibeLista(bancoDePalavrasDeTeste.getListaMetadePiorDePalavrasDeTeste()));
    }
//	

    @Test
    public void testeSeparaMetadeMelhorPalavras() {
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
        Gerador gerador = new Gerador();
        PalavraAlvo palavraAlvo = new PalavraAlvo();
        palavraAlvo.adicionaValores(0.7);
        palavraAlvo.adicionaValores(0.2);
        palavraAlvo.adicionaValores(0.4);
        palavraAlvo.adicionaValores(0.9);
        palavraAlvo.adicionaValores(0.3);
        int quantidadeDePalavrasDeTeste = 100;
        gerador.addPalavraAlvo(palavraAlvo);
        bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidadeDePalavrasDeTeste));
        BancoDeNPalavrasDeTeste banco = bancoDePalavrasDeTeste;
        ControleDeQualidadeDePalavrasDeTeste controladorDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
        controladorDeQualidade.ordenaPalavrasDeTeste(bancoDePalavrasDeTeste);
        bancoDePalavrasDeTeste = controladorDeQualidade.getBancoOrdenado();
        bancoDePalavrasDeTeste.addListaMetadeMelhorDePalavrasDeTeste(controladorDeQualidade.extraiMetadeMelhorDasPalavras(bancoDePalavrasDeTeste));

        assertEquals(0, bancoDePalavrasDeTeste.exibeMelhorLista());
    }
//	

    @Test
    public void testeSubstituiMetadePiordePalavrasDeTeste() {
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
        Gerador gerador = new Gerador();
        PalavraAlvo palavraAlvo = new PalavraAlvo();
        palavraAlvo.adicionaValores(0.7);
        palavraAlvo.adicionaValores(0.2);
        palavraAlvo.adicionaValores(0.4);
        palavraAlvo.adicionaValores(0.9);
        palavraAlvo.adicionaValores(0.3);
        int quantidadeDePalavrasDeTeste = 100;
        gerador.addPalavraAlvo(palavraAlvo);
        bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidadeDePalavrasDeTeste));
        BancoDeNPalavrasDeTeste banco = bancoDePalavrasDeTeste;
        ControleDeQualidadeDePalavrasDeTeste controladorDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
        controladorDeQualidade.ordenaPalavrasDeTeste(bancoDePalavrasDeTeste);
        bancoDePalavrasDeTeste = controladorDeQualidade.getBancoOrdenado();
        bancoDePalavrasDeTeste.addListaMetadeMelhorDePalavrasDeTeste(controladorDeQualidade.extraiMetadeMelhorDasPalavras(bancoDePalavrasDeTeste));
        bancoDePalavrasDeTeste.addListaMetadePiorDePalavrasDeTeste(controladorDeQualidade.extraiMetadePiorDasPalavras(bancoDePalavrasDeTeste));
        bancoDePalavrasDeTeste.addListaMetadePiorDePalavrasDeTeste(controladorDeQualidade.substituiMetadePiorDePalavrasDeTeste(bancoDePalavrasDeTeste.getListaMetadePiorDePalavrasDeTeste(), bancoDePalavrasDeTeste.getListaMetadeMelhorDePalavrasDeTeste(), gerador, 0.0, random));
        assertEquals(0, banco.exibePiorLista());
    }
    
    @Test
    public void testeMelhorPalavraDeTesteDentroDoIntervalo() {
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
        Gerador gerador = new Gerador();
        PalavraAlvo palavraAlvo = new PalavraAlvo();
        palavraAlvo.adicionaValores(0.7);
        palavraAlvo.adicionaValores(0.2);
        palavraAlvo.adicionaValores(0.4);
        palavraAlvo.adicionaValores(0.9);
        palavraAlvo.adicionaValores(0.3);
        int quantidadeDePalavrasDeTeste = 100;
        gerador.addPalavraAlvo(palavraAlvo);
        bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidadeDePalavrasDeTeste));
        ControleDeQualidadeDePalavrasDeTeste controladorDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
        controladorDeQualidade.ordenaPalavrasDeTeste(bancoDePalavrasDeTeste);
        bancoDePalavrasDeTeste = controladorDeQualidade.getBancoOrdenado();
        bancoDePalavrasDeTeste.addListaMetadeMelhorDePalavrasDeTeste(controladorDeQualidade.extraiMetadeMelhorDasPalavras(bancoDePalavrasDeTeste));
        bancoDePalavrasDeTeste.addListaMetadePiorDePalavrasDeTeste(controladorDeQualidade.extraiMetadePiorDasPalavras(bancoDePalavrasDeTeste));
        bancoDePalavrasDeTeste.addListaMetadePiorDePalavrasDeTeste(controladorDeQualidade.substituiMetadePiorDePalavrasDeTeste(bancoDePalavrasDeTeste.getListaMetadePiorDePalavrasDeTeste(), bancoDePalavrasDeTeste.getListaMetadeMelhorDePalavrasDeTeste(), gerador, 0.0, random));
        PalavraDeTeste melhorPalavraDeTesteDaIteracao = controladorDeQualidade.getMelhorSolucaoPorIteracao(bancoDePalavrasDeTeste.getListaMetadePiorDePalavrasDeTeste(), palavraAlvo);
        assertEquals(0.0000,melhorPalavraDeTesteDaIteracao.getDiferencaReferentePalavraAlvo(),0);
    }
    


    

}
