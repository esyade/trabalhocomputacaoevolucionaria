import static org.junit.Assert.*;

import org.junit.Test;

public class TesteCoordenadorLogica {

	@Test
	public void testAddBancoDeNPalavrasDeTeste() {
		CoordenadorLogica coordenadorLogica = new CoordenadorLogica();
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		coordenadorLogica.addBancoDePalavrasDeTeste(bancoDePalavrasDeTeste);
		assertEquals(0,coordenadorLogica.getBancoDePalavrasDeTeste());
	}
	
	@Test
	public void testAddControleDeQualidade() {
		CoordenadorLogica coordenadorLogica = new CoordenadorLogica();
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		coordenadorLogica.addBancoDePalavrasDeTeste(bancoDePalavrasDeTeste);
        ControleDeQualidadeDePalavrasDeTeste controleDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
		coordenadorLogica.addControleDeQualidade(controleDeQualidade);
		assertEquals(0,coordenadorLogica.getControleDeQualidade());
	}
	
	@Test
	public void testAddPAlavraAlvo() {
        PalavraAlvo palavraAlvo = new PalavraAlvo();
		CoordenadorLogica coordenadorLogica = new CoordenadorLogica();
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		coordenadorLogica.addBancoDePalavrasDeTeste(bancoDePalavrasDeTeste);
        ControleDeQualidadeDePalavrasDeTeste controleDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
		coordenadorLogica.addControleDeQualidade(controleDeQualidade);
		coordenadorLogica.addPalavraAlvo(palavraAlvo);
		assertEquals(0,coordenadorLogica.getPalavraAlvo());
	}
	
	@Test
	public void testeAddGerador(){
        Gerador gerador = new Gerador();
        PalavraAlvo palavraAlvo = new PalavraAlvo();
		CoordenadorLogica coordenadorLogica = new CoordenadorLogica();
        BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		coordenadorLogica.addBancoDePalavrasDeTeste(bancoDePalavrasDeTeste);
        ControleDeQualidadeDePalavrasDeTeste controleDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
		coordenadorLogica.addControleDeQualidade(controleDeQualidade);
		coordenadorLogica.addPalavraAlvo(palavraAlvo);
		assertEquals(0,coordenadorLogica.getPalavraAlvo());
	}

}
