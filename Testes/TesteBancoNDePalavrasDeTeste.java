import static org.junit.Assert.*;

import org.junit.Test;

public class TesteBancoNDePalavrasDeTeste {

	@Test
	public void testAddListaPalavrasDeTesteEmBanco() {
		BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		Gerador gerador = new Gerador();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		int quantidade = 100 ;
		gerador.addPalavraAlvo(palavraAlvo);
		bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidade));
		assertEquals(0,bancoDePalavrasDeTeste.exibeLista());
	}
//	
//	@Test
//	public void testExibesListaPalavrasDeTesteEmBanco() {
//		BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
//		GeradorDePalavrasDeTeste geradorDePalavrasDeTeste = new GeradorDePalavrasDeTeste();
//		PalavraAlvo palavraAlvo = new PalavraAlvo();
//		palavraAlvo.adicionaValores(0.7);
//		palavraAlvo.adicionaValores(0.2);
//		palavraAlvo.adicionaValores(0.4);
//		palavraAlvo.adicionaValores(0.9);
//		palavraAlvo.adicionaValores(0.3);
//		geradorDePalavrasDeTeste.addTamanhoDePalavraDeTeste(palavraAlvo.tamanho());
//		geradorDePalavrasDeTeste.addGeradorDeValores(new Gerador());
//		int quantidade = 100 ;
//		bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(geradorDePalavrasDeTeste.geradorDeNumerosReais.gerarPalavrasDeTeste(geradorDePalavrasDeTeste, quantidade));
//		assertEquals(0,bancoDePalavrasDeTeste.getListaDePalavrasDeTeste());
//	}

}
