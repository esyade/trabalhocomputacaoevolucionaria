import static org.junit.Assert.*;

import org.junit.Test;

public class TestePalavraAlvo {
	

	
	@Test
	public void addPalavraAlvoT5() {
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		assertEquals(5,palavraAlvo.tamanho());
	}
	
	@Test
	public void TesteGetValorPalavraAlvo() {
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		assertEquals(0.7,palavraAlvo.getLetra(0),0);
	}
	
	@Test
	public void testToleranciaDePalavraAlvoParaMenos(){
		BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		palavraAlvo.gerarToleranciaPalavraAlvo();
		assertEquals(0,palavraAlvo.getToleranciaParaMenos(0),0);		
	}
	
	@Test
	public void testToleranciaDePalavraAlvoParaMais(){
		BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		palavraAlvo.gerarToleranciaPalavraAlvo();
		assertEquals(0,palavraAlvo.getToleranciaParaMais(0),0);		
	}
	
	@Test
	public void testMaiorValorToleranciaDePalavraAlvoParaMais(){
		BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		palavraAlvo.gerarToleranciaPalavraAlvo();
		assertEquals(0,palavraAlvo.getMaiorValorToleranciaParaMais(),0);		
	}
	
	@Test
	public void testMenorValorToleranciaDePalavraAlvoParaMenos(){
		BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.70);
		palavraAlvo.adicionaValores(0.20);
		palavraAlvo.adicionaValores(0.40);
		palavraAlvo.adicionaValores(0.90);
		palavraAlvo.adicionaValores(0.30);
		palavraAlvo.gerarToleranciaPalavraAlvo();
		assertEquals(4,palavraAlvo.getMenorValorToleranciaParMenos(),0);		
	}


}
