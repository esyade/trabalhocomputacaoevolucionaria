import static org.junit.Assert.*;

import org.junit.Test;
/*
 * A palavra de teste deve ter o tamanho da palavra alvo
 * requisitos: tamanho da palavra alvo de 10 ou de 5
 * 
 * 
 * */
public class TestePalavraDeTeste {
	
	@Test
	public void addPalavraTesteT5() {
		PalavraDeTeste palavradeTeste = new PalavraDeTeste();
		palavradeTeste.adicionaValores(0.7);
		palavradeTeste.adicionaValores(0.2);
		palavradeTeste.adicionaValores(0.4);
		palavradeTeste.adicionaValores(0.9);
		palavradeTeste.adicionaValores(0.3);
		assertEquals(5,palavradeTeste.tamanho());
	}

}
