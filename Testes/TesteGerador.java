import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;
/*
 * Requisitos para o gerador:
 * 	Intervalo em que o gerador precisar gerar os n�meros...
 */
public class TesteGerador {

	@Test
	public void testGeradorDeValor() {
		Gerador gerador = new Gerador();
		assertEquals(0,gerador.gerarValor(), 0);
	}
	
	@Test
	public void testFormatacao(){
		Gerador gerador = new Gerador();
		gerador.gerarValor();
		double valorFormatado = gerador.getValor();
		assertEquals(1.00,valorFormatado,0);
	}
//	
	@Test
	public void testGeradorComGaussiana() {
		Gerador gerador = new Gerador();
		assertEquals(0, gerador.gerarValorPorGaussiana(0.54, 0,new Random()), 0);
	}
	
	@Test
	public void testGerarPalavraDeTeste() {
		Gerador gerador = new Gerador();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		gerador.addPalavraAlvo(palavraAlvo);
		assertEquals(0,gerador.exibePalavraDeTeste(gerador.gerarPalavraDeTeste()));
	}
	
	@Test
	public void testGerarListaDePalavraDeTeste() {
		Gerador gerador = new Gerador();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		gerador.addPalavraAlvo(palavraAlvo);
//		List<PalavraDeTeste> lista = new ArrayList<>();
//		for(int i = 0 ; i < 50 ; i++){
//			lista.add(gerador.gerarPalavraDeTeste());
//		}
		assertEquals(0,gerador.exibeLista(gerador.gerarListaDePalavrasDeTeste(50)));
	}
	

	@Test
	public void testGerarPalavraDeTesteGaussiana() {
		Gerador gerador = new Gerador();
		Random random = new Random();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		gerador.addPalavraAlvo(palavraAlvo);
//		List<PalavraDeTeste> lista = new ArrayList<>();
//		for(int i = 0 ; i < 50 ; i++){
//			lista.add(gerador.gerarPalavraDeTeste());
//		}
		assertEquals(0,gerador.exibePalavraDeTeste(gerador.gerarPalavraDeTesteGaussiana(0.7, 0,random)));
	}
	
	@Test
	public void testGerarListaPalavraDeTesteGaussiana() {
		BancoDeNPalavrasDeTeste bancoDePalavrasDeTeste = new BancoDeNPalavrasDeTeste();
		Gerador gerador = new Gerador();
		PalavraAlvo palavraAlvo = new PalavraAlvo();
		palavraAlvo.adicionaValores(0.7);
		palavraAlvo.adicionaValores(0.2);
		palavraAlvo.adicionaValores(0.4);
		palavraAlvo.adicionaValores(0.9);
		palavraAlvo.adicionaValores(0.3);
		int quantidadeDePalavrasDeTeste = 50 ;
		gerador.addPalavraAlvo(palavraAlvo);
		bancoDePalavrasDeTeste.addListaDePalavrasDeTeste(gerador.gerarListaDePalavrasDeTeste(quantidadeDePalavrasDeTeste));
		BancoDeNPalavrasDeTeste banco = bancoDePalavrasDeTeste ;
		ControleDeQualidadeDePalavrasDeTeste controladorDeQualidade = new ControleDeQualidadeDePalavrasDeTeste();
		controladorDeQualidade.ordenaPalavrasDeTeste(bancoDePalavrasDeTeste);
		bancoDePalavrasDeTeste = controladorDeQualidade.getBancoOrdenado();
		bancoDePalavrasDeTeste.addListaMetadeMelhorDePalavrasDeTeste(controladorDeQualidade.extraiMetadeMelhorDasPalavras(bancoDePalavrasDeTeste));
		bancoDePalavrasDeTeste.addListaMetadePiorDePalavrasDeTeste(controladorDeQualidade.extraiMetadePiorDasPalavras(bancoDePalavrasDeTeste));
		List<PalavraDeTeste> lTeste = new ArrayList<>();
		Random random = new Random();
		for(int w = 0 ; w < 5 ; w++){
			for(int i = 0 ; i < bancoDePalavrasDeTeste.getListaMetadePiorDePalavrasDeTeste().size() ; i++){
				PalavraDeTeste p = bancoDePalavrasDeTeste.getListaMetadePiorDePalavrasDeTeste().get(i);
				PalavraDeTeste pTeste = new PalavraDeTeste();
				for(int j = 0 ; j < p.tamanho() ; j++){
					pTeste.adicionaValores(gerador.gerarValorPorGaussiana(p.getLetra(j), 0,random));
				}
				lTeste.add(pTeste);
			}
		}
		assertEquals(0.0, gerador.exibeLista(lTeste));
	}
	
	

}
